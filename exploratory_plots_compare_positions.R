library(tidyverse)

# Exploratory plotting for OSR simulations of Burridge coring location project.
# Plots with no filename noted in comments are ones that didn't seem useful enough to bother saving *at the time*,
# but we should remake them freely if needed

input_dir <- file.path("Burridge_coring_20190508")
output_dir <- file.path("")

# provides these functions:
# parse_filenames, read_coredat, read_tabledat, coredat_to_layers
source(data_readers.R)

coredat <- read_coredat(parse_filenames(input_dir, "coringData.*.tab"))
coredat_layered <- coredat_to_layers(coredat)

tabledat <- read_tabledat(parse_filenames(input_dir, "tabled_output.tab"))
tabledat_layered <- tabledat_to_layers(tabledat)

tabledat_cores <- (
	tabledat
	%>% filter(grepl("Coring$", path))
	%>% separate(
		col = "name",
		into = c("location", "diameter_class"),
		sep = "_")
)

coredat_sums <- (
	coredat_layered
	%>% filter(diameter_class=="tot")
	%>% group_by(crop, root_angle, sampling_scheme, location, replicate)
	%>% summarize( # collapsing layers within each core
		root_length_cm = sum(root_length_cm),
		core_length_m = sum(layer_thickness_m),
		core_area_m2 = unique(core_area_m2),
		core_RLD_cm_m3 = root_length_cm / (core_length_m * core_area_m2))
)

tabledat_sums <- (
	tabledat_layered
	%>% filter(depth_top_cm < 60) # to match coring depth
	%>% group_by(crop, root_angle, replicate)
	%>% summarize(
		root_length_cm = sum(value),
		plot_depth_m = sum(layer_thickness_m),
		plot_area_m2 = unique(plot_area_m2),
		plot_RLD_cm_m3 = root_length_cm / (plot_depth_m * plot_area_m2))
)

both_sums <- coredat_sums %>% left_join(tabledat_sums, by = c("crop", "root_angle", "replicate"))


# Check distribution of coring locations:
# random all over plot, fixed all in 6 locs
# Also shows that fixed bean coring locations are probably not optimal --
# highest-x-dimension cores are basically on top of next plant in row,
# whereas in maze they're midway to it
(ggplot(
		filter(coredat, `time(d)` == 40, diameter_class == "tot"),
		aes(x, z, color = sampling_scheme))
	+ geom_point(alpha = 0.2)
	+ facet_wrap(crop~root_angle)
	+ coord_fixed())


# Bean rootLength on day 40 always 22.30476 cm greater than
# sum of RootLengthProfile layers?!
# Maize is about 385-400 cm higher, with no clear pattern in the residual
# One guess: Maybe difference comes from above-ground roots?
# For consistency, I'll sum up layers instead of using rootLength for all uses of "total root length" below
(tabledat
	%>% filter(time == 40, name == "rootLength")
	%>% group_by(crop, root_angle, replicate)
	%>% summarize(value = sum(value))
	%>% full_join(
		(tabledat_layered
			%>% group_by(crop, root_angle, replicate)
			%>% summarize(value = sum(value))),
		by = c("crop", "root_angle", "replicate"),
		suffix = c("_total", "_layersum"))
	%>% ggplot(aes(value_total, value_layersum - value_total))
		+ geom_point()
		+ facet_wrap(crop~root_angle, scales = "free")
)

# Total root length in the whole plot, summing across all 4 plants and all layers:
# Fairly consistent between angles for bean, maize shallow a little less than other maize
# rootlength_plot_tot.png
(tabledat_layered
	%>% group_by(crop, root_angle, replicate)
	%>% summarize(root_length_cm = sum(value))
	%>% ggplot(aes(root_angle, root_length_cm))
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary()
		+ facet_wrap(~crop, scales="free_y")
		+ theme_bw()
		+ ggtitle("total root length from all 4 plants, 0-150 cm"))

#... but total bean root length in the top 60 cm, where cores are taken, differs a *lot*!
# ==> If cores are effective at distinguishing between phenotypes, we should
# expect to see *less root length in the whole core* from deeper-rooted beans
# rootlength_plot_tot_0-60.png
(tabledat_layered
	%>% filter(depth_top_cm < 60)
	%>% group_by(crop, root_angle, replicate)
	%>% summarize(root_length_cm = sum(value))
	%>% ggplot(aes(root_angle, root_length_cm))
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary()
		+ facet_wrap(~crop, scales="free_y")
		+ theme_bw()
		+ ggtitle("total root length from all 4 plants, 0-60 cm"))


# Some profile plots
# p much strictly exploring here
(# all locations as separate lines
	ggplot(
		coredat_layered %>% filter(diameter_class == "tot"),
		aes(depth_top_cm, root_length_density_cm_m3))
	+ geom_line(aes(group = paste(replicate, location)), alpha = 0.2)
	+ facet_wrap(sampling_scheme ~ crop ~ root_angle, scales = "free_y")
	+ coord_flip()
	+ scale_x_reverse()
	+ theme_bw()
)
# fixed locations only, facetting by location
# fixed_loc_profiles_20190509.png
(	ggplot(
		coredat_layered %>% filter(sampling_scheme == "fixed", diameter_class == "tot"),
		aes(depth_top_cm, root_length_density_cm_m3, group=replicate))
	+ geom_line(alpha = 0.2)
	+ geom_line(
		data = tabledat_layered %>% filter(depth_top_cm < 60),
		color = "red")
	+ facet_grid(crop ~ root_angle ~ location)
	+ coord_flip()
	+ scale_x_reverse()
	+ theme_bw())

# whole-profile RLD for fixed cores vs whole plot
# core_sum_diffs_fixed.png
(both_sums
	%>% filter(sampling_scheme == "fixed")
	%>% mutate(trt = paste(crop, root_angle))
	%>% ggplot(aes(location, core_RLD_cm_m3 - plot_RLD_cm_m3))
		+ geom_hline(aes(yintercept = 0), color = "red")
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(
			geom = "point",
			fun.y = "mean")
		+ coord_flip()
		+ facet_wrap(~trt, scales = "free_x")
		+ theme_bw()
		+ ggtitle("Difference in 0-60 cm RLD between core and whole plot (n=100; line=median, dot=mean)"))

# And also for random cores
# core_sum_diffs_rand.png
(both_sums
	%>% filter(sampling_scheme == "random")
	%>% mutate(trt = paste(crop, root_angle))
	%>% ggplot(aes(location, core_RLD_cm_m3 - plot_RLD_cm_m3))
		+ geom_hline(aes(yintercept = 0), color = "red")
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(
			geom = "point",
			fun.y = "mean")
		+ coord_flip()
		+ facet_wrap(~trt, scales = "free_x")
		+ theme_bw()
		+ ggtitle("Difference in 0-60 cm RLD between core and whole plot (n=100; line=median, dot=mean)"))


# replicate means: averaging *within layers* across some or all locations
# NB location subsets will change each time this is run
layer_means <- function(dat, n, varname = "root_length_density_cm_m3") {
	varname <- sym(varname)
	resultname <- paste0("RLD_", n)
	locs <- sample(unique(dat$location), size = n)
	return(dat
		%>% filter(location %in% locs)
		%>% group_by(depth_top_cm)
		%>% summarize(!!resultname := mean(!!varname)))
}
layer_repmeans <- (coredat_layered
	%>% filter(diameter_class == "tot")
	%>% group_by(crop, root_angle, sampling_scheme, replicate)
	%>% group_modify( #NB requires dplyr >= 0.8.1
		~ (.x
			%>% group_by(depth_top_cm)
			%>% summarize(RLD_all = mean(root_length_density_cm_m3))
			%>% left_join(layer_means(.x, 1), by = "depth_top_cm")
			%>% left_join(layer_means(.x, 2), by = "depth_top_cm")
			#%>% left_join(layer_means(.x, 3), by = "depth_top_cm")
			%>% left_join(layer_means(.x, 4), by = "depth_top_cm")
			#%>% left_join(layer_means(.x, 5), by = "depth_top_cm")
			%>% left_join(layer_means(.x, 6), by = "depth_top_cm")))
	%>% ungroup()
	%>% left_join(tabledat_layered)
	%>% rename(RLD_true = root_length_density_cm_m3)
	%>% select(crop, root_angle, sampling_scheme, replicate, depth_top_cm, starts_with("RLD"))
	%>% gather("method", "RLD", starts_with("RLD"))
)

(# replicate means: pools locations *within each layer*
	ggplot(
		filter(layer_repmeans, method != "RLD_true"),
		aes(x = depth_top_cm,
			y = RLD))
	+ geom_line(
		aes(group = paste(method, replicate), color = "cores"),
		alpha = 0.1)
	+ geom_line(
		data = (layer_repmeans
			%>% filter(method == "RLD_true")
			%>% select(-method)),
		aes(group = replicate, color = "true"))
	+ facet_grid(paste(crop, root_angle) ~ sampling_scheme ~ method)
	+ coord_flip()
	+ scale_x_reverse()
	+ theme_bw()
	+ ggtitle("replicate means")
	+ scale_color_manual(
		name = "estimate source",
		values = c(cores = "black", true = "red"))
)

# The above is (1) hard to read with so many panels, and
# (2) a bit unfair to the fixed sampling because e.g. RLD_2 is "mean of 2 cores randomly selected from the 6 fixed locations",
# which we hope nobody's proposing.
# Here, let's zoom in on just RLD_6 -- so this is "mean of all 6 fixed locations" vs "mean of 6 random locations"
# rand_vs_fixed_repmeans_20190509.png
(
	ggplot(
		filter(layer_repmeans, method == "RLD_6"),
		aes(x = depth_top_cm,
			y = RLD))
	+ geom_line(
		aes(group = paste(method, replicate), color = "cores"),
		alpha = 0.1)
	+ geom_line(
		data = (layer_repmeans
			%>% filter(method == "RLD_true")
			%>% select(-method)),
		aes(group = replicate, color = "true"))
	+ facet_grid(crop ~ root_angle ~ sampling_scheme)
	+ coord_flip()
	+ scale_x_reverse()
	+ theme_bw()
	+ ggtitle("replicate means of 6 locations")
	+ scale_color_manual(
		name = "estimate source",
		values = c(cores = "black", true = "red"))
)

# collapsing profiles to look at estimated total root length for whole profile
# NB expressing as sum of RLD, not sum of raw root length --
# This gives nonsense units (more or less "the root density you'd get if you took
# all the roots from the whole profile and squished them into one layer")
# but should be proportional to raw root length since all are divided by same volume
# and this way still normalized for easy comparison to whole-plot truth
# To do this right, go back and rebuild layer_repmeans with length instead of RLD
(layer_repmeans
	%>% group_by(crop, root_angle, sampling_scheme, replicate, method)
	%>% summarize(RLD_sum = sum(RLD))
	%>% ggplot(
		aes(method, RLD_sum, fill = sampling_scheme))
		+ geom_violin(scale="width", draw_quantiles=0.5)
		+ stat_summary(
			geom="point",
			fun.y="mean",
			position=position_dodge(1))
		+ facet_grid(crop ~ root_angle)
		+ scale_y_log10()
		+ theme_bw()
		+ ggtitle("profile sum RLD (n=100; line=median, dot=mean; violins scaled to equal width)"))

# Here, let's do it right like I said above
# Still using RLD, but RLD calculated for whole profile instead of summing RLD from individual layers
# Also still true that "mean of two randomly selected from 6 fixed locations" is a bad sampling protocol,
# but at least lets us see how means of random samples converge to true and fixed samples don't
mean_n <- function(x, n){
	stopifnot(length(x) >= n)
	mean(sample(x, size=n))
}
means_of_sums <- (both_sums
	%>% group_by(crop, root_angle, sampling_scheme, replicate)
	%>% rename(RLD = core_RLD_cm_m3)
	%>% summarize(
		m_1 = mean_n(RLD, 1),
		m_2 = mean_n(RLD, 2),
		m_3 = mean_n(RLD, 3),
		m_4 = mean_n(RLD, 4),
		m_5 = mean_n(RLD, 5),
		m_6 = mean_n(RLD, 6),
		m_all = mean(RLD),
		true = mean(plot_RLD_cm_m3))
	%>% gather("method", "profile_RLD", one_of(paste0("m_", 1:6), "m_all", "true")))
# rand_vs_fixed_profile_sums.png
(ggplot(means_of_sums,
	aes(method, profile_RLD, fill = sampling_scheme))
	+ geom_violin(scale = "width", draw_quantiles = 0.5)
	+ stat_summary(geom = "point", fun.y = "mean", position = position_dodge(1))
	+ facet_grid(crop ~ root_angle, scales = "free_y")
	+ theme_bw()
	+ ggtitle("0-60 cm RLD, mean of 1-6 locations (n=100; line=median, dot=mean; violins scaled to equal width)"))
# ...But if we zoom in on the 6-or-15-location means, we see bad news:
# Even the mean of 15 random locations isn't precise enough to reliably distinguish between
# the whole-profile RLDs of deep and shallow bean!
# rand_vs_fixed_profile_sums_byangle.png
(ggplot(means_of_sums %>% filter(method %in% c("m_6", "m_all", "true")),
		aes(root_angle, profile_RLD, fill=sampling_scheme))
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(geom = "point", fun.y = "mean", position = position_dodge(1))
		+ facet_grid(crop~method, scales="free_y")
		+ theme_bw()
		+ ggtitle("0-60 cm RLD, mean of 6 or 15 locations (n=100; line=median, dot=mean)"))

# differences from true RLD
# Again, this isn't really the right dataset to use for these;
# see below for an approach that doesn't pretend we can sum RLD
layer_diff <- (layer_repmeans
	%>% spread("method", "RLD")
	%>% mutate_at(vars(starts_with("RLD")), ~ . - RLD_true))

(ggplot(
	data = (layer_diff
		%>% select(-RLD_true)
		%>% gather("method", "RLD_err", starts_with("RLD"))),
	aes(depth_top_cm, RLD_err, color = method))
	+ geom_smooth(method = "loess")
	+ facet_grid(sampling_scheme ~ paste(crop, root_angle))
	+ theme_bw())
# ... probably easier to interpret as a percent difference (produces same lines, different axis values):
(ggplot(
	data = (layer_repmeans
		%>% spread("method", "RLD")
		%>% mutate_at(vars(starts_with("RLD")), ~ 100 * (. - RLD_true)/RLD_true)
		%>% select(-RLD_true)
		%>% gather("method", "RLD_pct_err", starts_with("RLD"))),
	aes(depth_top_cm, RLD_pct_err, color = method))
    + geom_smooth(method = "loess")
	+ facet_grid(sampling_scheme ~ paste(crop, root_angle))
	+ theme_bw())

# Differences from true density
# idea: sum (absolute) differences within each profile
# then take mean across replicates
(# sum errors within each profile: "how well do we estimate total root mass?"
	layer_diff
	%>% group_by(crop, root_angle, sampling_scheme, replicate)
	%>% summarize_at(vars(starts_with("RLD")), ~sum(.))
	%>% select(-RLD_true)
	%>% gather("method", "RLD_err", starts_with("RLD"))
	%>% ggplot(aes(method, RLD_err, fill = sampling_scheme))
		+ geom_hline(yintercept = 0, linetype = "dashed")
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(geom = "point", fun.y="mean", position = position_dodge(1))
		+ facet_grid(root_angle~crop)
		+ theme_bw())
(# sum absolute errors within each profile: "How well do we estimate the shape of the profile?"
	layer_diff
	%>% group_by(crop, root_angle, sampling_scheme, replicate)
	%>% summarize_at(vars(starts_with("RLD")), ~sum(abs(.)))
	%>% select(-RLD_true)
	%>% gather("method", "RLD_abs_err", starts_with("RLD"))
	%>% ggplot(aes(method, RLD_abs_err, fill = sampling_scheme))
		+ geom_hline(yintercept = 0, linetype = "dashed")
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(geom = "point", fun.y="mean", position = position_dodge(1))
		+ facet_grid(root_angle~crop)
		+ theme_bw())

# RLD error *within layers*, like I should have done earlier
both_layered <- (coredat_layered
	%>% filter(diameter_class == "tot")
	%>% left_join(
		tabledat_layered,
		by = c("crop", "root_angle", "replicate", "depth_top_cm", "depth_bottom_cm"),
		suffix = c("_core", "_plot"))
	%>% mutate(
		RLD_err = root_length_density_cm_m3_core - root_length_density_cm_m3_plot,
		RLD_err_percent = RLD_err / root_length_density_cm_m3_plot * 100))

# RLD_err_pct_profile.png
(ggplot(
	both_layered %>% filter(sampling_scheme == "fixed" | location == "rand01"),
	aes(depth_top_cm, RLD_err_percent, group=replicate))
	+ geom_line(alpha=0.3)
	+ facet_grid(crop ~ root_angle ~ location, scales = "free_x")
	+ coord_flip()
	+ scale_x_reverse()
	+ theme_bw())

# Collapse layers of each core into a one-number error metric
#
# I calculate a bunch of metrics here, but am not convinced they all make sense here:
# Keep in mind this step is just collapsing (correlated) layers within a single core,
# NOT between replicates at a location.
# Also I do the calculations both on raw difference between core and plot,
# and on *percent error* of each layer ==> obs=1.5, true=1.0 is treated as
# same size error as obs=150, true=100 ==> error magnitude not dominated by raw RLD
# ==> For most purposes, probably want to use the percent versions
#
# Of all these metrics, pct_MAE makes the most sense to me when thinking about all depths of one core.
# If individual layers differed from reference by +30%, +20%, -10%, +10%, -20%, -10%,
# then pct_MAE says "the core deviated from the truth by 17% on average"
#
# Other metrics may be more appropriate for summarizing across *replicates*
# e.g. maybe we'll want to compare "the RMSE of all 100 pct_MAE values" between locations??
both_err_metrics <- (both_layered
	%>% group_by(crop, root_angle, replicate, sampling_scheme, location)
	%>% summarize(
		pct_bias = mean(RLD_err_percent),
		pct_MAE = mean(abs(RLD_err_percent)),
		pct_MSE = mean(RLD_err_percent^2),
		pct_RMSE = sqrt(pct_MSE),
		bias = mean(RLD_err),
		MAE = mean(abs(RLD_err)),
		MSE = mean(RLD_err^2),
		RMSE = sqrt(MSE),
		euclidian_distance = unclass(dist(
			x = rbind(root_length_density_cm_m3_core, root_length_density_cm_m3_plot),
			method = "euclidian")),
		manhattan_distance = unclass(dist(
			x = rbind(root_length_density_cm_m3_core, root_length_density_cm_m3_plot),
			method = "manhattan")))
)

# percent error version of core_sum_diffs_fixed.png
# Also added one random location for comparison
# pct_sumerr_fixed.png
(both_sums
	%>% filter(sampling_scheme == "fixed" | location=="rand01")
	%>% mutate(trt = paste(crop, root_angle))
	%>% ggplot(aes(location, (core_RLD_cm_m3 - plot_RLD_cm_m3)/plot_RLD_cm_m3 * 100))
		+ geom_hline(aes(yintercept = 0), color = "red")
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(
			geom = "point",
			fun.y = "mean")
		+ coord_flip()
		+ facet_wrap(~trt, scales = "free_x")
		+ theme_bw()
		+ ggtitle("Percent difference in 0-60 cm RLD between core and whole plot (n=100; line=median, dot=mean)"))
# ditto but using profile MAE instead of RLD sum
# pct_MAE_fixed.png
(both_err_metrics
	%>% filter(sampling_scheme == "fixed" | location=="rand01")
	%>% mutate(trt = paste(crop, root_angle))
	%>% ggplot(aes(location, pct_MAE))
		#+ geom_hline(aes(yintercept = 0), color = "red")
		+ geom_violin(draw_quantiles = 0.5)
		+ stat_summary(
			geom = "point",
			fun.y = "mean")
		+ coord_flip()
		+ facet_wrap(~trt, scales = "free_x")
		+ theme_bw()
		+ ggtitle("profile mean of |% RLD error| between core and whole plot (n=100; line=median, dot=mean)"))
