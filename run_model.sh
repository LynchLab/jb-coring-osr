#!/bin/bash

#PBS -A open
#PBS -l nodes=1:ppn=1
#PBS -l walltime=01:00:00
#PBS -l pmem=6gb
#PBS -j oe

set -e

# When called directly: `./run_one.sh path/to/infile.xml`
# When called by qsub: `qsub [q_args] -v 'file=path/to/infile.xml' run.sh`
if [ -z ${file} ]; then
	file=$1
fi
if [ -z ${PBS_O_WORKDIR} ]; then
	PBS_O_WORKDIR=$(pwd)
fi

PARAM_FILE=$(basename ${file})
RUN_DIR=$(dirname ${file})

# Caution: Must use OSR binary with fix for multiple-plant D95 calculations,
# or all reported D95 (and D80, D50, etc) will be incorrect
OPENSIMROOT=${PBS_O_WORKDIR}/OSR_D95fix_af8addf_20190418

BATCH_LOG=${PBS_O_WORKDIR}/qsub_log.txt

TIME=`date +%s`
cd ${PBS_O_WORKDIR}/${RUN_DIR}
echo "${PARAM_FILE} start ${TIME}" >> ${BATCH_LOG}

(time ${OPENSIMROOT} ${PARAM_FILE}) >log_${TIME}.txt 2>&1

ENDTIME=`date +%s`
echo "${PARAM_FILE} done ${ENDTIME} ("$((ENDTIME - TIME))" sec)" >> ${BATCH_LOG}
