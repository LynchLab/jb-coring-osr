
## 2019-03-18

Generating fresh input files for the coring study -- some OpenSimRoot parameters have changed their defaults since the existing inputs were generated, so my goal is to start from current defaults and then add the experiment-specific modifications.

### Generating default inputs

using Ernst's Python input-zipper on the maize and bean files from revision 224c6766 of the OpenSimRoot public master branch:

```{bash}
git clone git@gitlab.com:rootmodels/opensimroottools.git osrTools
cd ~/osrTools
python3
```

```{python3}
from GeneralFunctions import *
from FunctionsXML import *
mz = ReadFile("/Users/chrisb/OpenSimRoot/OpenSimRoot/InputFiles/runMaize.xml")
mz = CleanZipFile(mz, "/Users/chrisb/OpenSimRoot/OpenSimRoot/InputFiles/")
mz = CondenseSimulaBases(mz)
with open("/Users/chrisb/Desktop/Burridge_coring_20190318/maize.xml", "wb") as f:
	f.write(mz.encode("UTF-8"))
	f.close()
bn = ReadFile("/Users/chrisb/OpenSimRoot/OpenSimRoot/InputFiles/runBean.xml")
bn = CleanZipFile(bn, "/Users/chrisb/OpenSimRoot/OpenSimRoot/InputFiles/")
bn = CondenseSimulaBases(bn)
with open("/Users/chrisb/Desktop/Burridge_coring_20190318/bean.xml", "wb") as f:
	f.write(bn.encode("UTF-8"))
	f.close()
```

I predict I'll eventually want a wrapper script for this pattern, so that I can call it as e.g. `./CleanZipXML.py path/to/infile.xml outfile.xml`. For today, this works.

### Rebuilding the coring treatments

My approach here is inefficient, but is the easiest way I immediately see to merge in the changes that matter without accidentally bringing along bad values: I will review differences between the coring project input files and the master branch input files, then copy over the ones that are needed for the experiment:

* Find which files differ at all with `diff -rq "~/Dropbox/Coring Study/Analysis/SimRoot/Output/fieldCores_rep1_maize/InputFiles" "~/github_forks/OpenSimRoot/OpenSimRoot/InputFiles"`
* Review the list of differing files and ignore differences that won't apply to this experiment. e.g. I know that `straightRoot.xml` is irrelevant to maize and bean simulations and that nothing in `environments/pots/` is used when environment parameters come from the Wageningen files, so I don't need to look at the line-by-line diffs for these.
* Compare line-by-line diffs where needed with `diff -u ...fieldCores_rep1_maize/InputFiles/...file.xml ...OpenSimRoot/InputFiles/...file.xml`, transfer only the changed lines that are directly related to the coring experiment.
* Once I've identified changes to make, make them by hand-editing `maize.xml` and `bean.xml`, with Git commits after each one to allow granular roolback if needed.

Not listing every file reviewed here, just the changes I made as I found them. "was" = value from default simulation, "now" = value for coring study.

* `/environment/dimensions`
	- bean minCorner was -11 -150 -15, now -5 -150 -38.1
	- bean maxCorner was 11 0 15, now 15 0 114.3
	- maize minCorner was -13 -150 -30, now -11.5 -150 -38.1
	- main maxCorner was 13 0 30, now 34.5 0 114.3
* `/environment/plantingScheme`
	- was not present, now added between `environment/dimensions` and `environment/soil`
	- both bean and maize specify 2 rows of 2 plants each, 150 cm profile depth, rows direction "x", row spacing 76.2 cm, plantingDepth -1.5 cm, plantingTime 0
	- inTheRowSpacing is 10 cm bean, 23 cm maize
	- bean plantType is "Bean-Carioca-SimRoot4", unchanged from default
	- maize plantType is now "36H56", was "maize-aerenchyma" in object generator template but this will get updated in a moment
* `soil/RootLengthProfile*`
	- was not present in bean (either default or previous coring design), now copied from maize
	- was already present in maize, not changed
* `soil/fieldCoring`
	- was not present in bean, now added after `/soil/RootLengthProfile_90+`
	- was not present in maize, now replaces `/soil/inTheRowCoring` and `/soil/betweenTheRowCoring`
	- specifies 6 coring locations: L1 0 0 5.5, L2 11 0 0, L3 0 0 11, L4 0 0 38.1, L5 11 0 38.1, L6 5.5 0 0. Positions are same in both bean and maize
	- each core is 60 cm deep and divided into 10 cm increments
	- each core reports root length from 3 diameter classes (0-0.02, 0.02-0.05. 0.05-10 cm) plus total from all diameters
* `simulationControls/outputParameters`
	- modelDump for day 1 was on in bean, now off. Was off in maize, not changed
	- VTP output was off for both, now enabled at 10-day intervals
	- VTU output was on at 1-day intervals for bean, off for maize. Now off for both
	- RSML output was on for maize, unspecified for bean. Now off for maize, still unspecified for bean.
* `rootTypeParameters/*/bounceOfTheSide`
	- was 1 for all classes in bean, 0 for all classes in maize. Now 1 for all in both.
* `/plants`
	- was a SimulaBase with no attributes, containing one child SimulaBase with objectGenerator "seedling" and grandchildren specifying plantType, plantingTime, plantPosition. Now just `<SimulaBase name="plants" objectGenerator="fieldPlanting" />` -- all the other info is specified in `environment/plantingScheme`, already updated above
* `plantTemplate/D[50, 80, 90, 95]`
	- was not present, now added to both maize and bean to report depth to 50, 80, 90, 95% of total root length.
* `rootTypeParameters`
	- In maize, this section was four SimulaBases (600+ lines each) giving root type parameters for maize varieties "H99", "W64a", "36H56", "maize-aerenchyma". Only 36H56 is used in this simulation, so deleted the other three.
* `dataPointTemplate/rootSegmentLengthInCore*`
	- was not present, now declares SimulaTables for root segment length inside each core. These show up in the VTP output, to allow visualizing / extracting root segments that fall within each core.
	- for the record, as far as I can tell these work by black magic: Each one is a SimulaTable with column1 as time and column 2 as length in cm, initially containing only "0 0" and with attribute `LLfunction="getValuesFromSWMS"`, but I can't find *anywhere* in the OSR source code that mentions `LLfunction`!

To make sure I found all the differences, compared these hand-constructed files against those used by main-testing.xml in Burridge_coring_20181203 (by zipping/condensing main-testing.xml uses Ernst's python approach as above, then diffing the results against bean.xml/maize.xml).

Found these differences:

* 2018 inputs put coring at top level (`//fieldCoring`), I have it in `//soil/fieldCoring`. Fixed.
* 2018 inputs set `//simulationControls/randomNumberGeneratorSeed`, I don't. Not changing this at the moment -- I expect the next step is to run a lot of copies of the same simulation, for which I'll want the RNG seed unset anyway.
* 2018 inputs started garbage collection at day 2, I start at 5. Not changed.
* 2018 inputs write VTP every 20 days, I write every 10. Fixed.
* New bean inputs include Barber-Cushman P model. Don't need this and it slows the model down a lot. Fixed.
* New maize inputs include aerenchyma, root hair, stress factor, and carbon submodules. 2018 do not. Not sure how much difference it makes in the coring output, but disabled for hopefully faster runtime.
* 2018 maize inputs had `bounceOfTheSide` disabled, I have it enabled. I think enabled is more correct, so not changed -- bounced roots will simulate roots entering from neighboring plants outside the simulated plot.


### Test runs

Have been doing some test runs as I go (details not shown) to monitor runtime and check for errors, but let's check the finished inputs a bit more carefully.

```
> mkdir beantest && cd beantest
> ~/OpenSimRoot/OpenSimRoot/StaticBuild/OSR_224c676_master_gcc7 ../bean.xml
Running OpenSimRoot build on Feb 28 2019, licensed GPLv3
Trying to load model from file:                                              OK
Running modules: 40.0/40.0 days. Mem 2209.0 mB. #obj.=3986023 x64b/obj.=72.6 OK
Finalizing output:                                                           OK
THE MODEL PUT OUT WARNINGS:
FieldPlanting: adjusting area per plant from 660.000000 to 762.000000        1x
threshingrootDiameter                                                   591290x
Simulation took (hours:minutes:seconds): 0:7:36
> mkdir maizetest && cd maizetest
> ~/OpenSimRoot/OpenSimRoot/StaticBuild/OSR_224c676_master_gcc7 ../maize.xml
Running OpenSimRoot build on Feb 28 2019, licensed GPLv3
Trying to load model from file:                                              OK
Running modules:40.0/40.0 days. Mem 5424.0 mB. #obj.=11358176 x64b/obj.=62.6 OK
Finalizing output:                                                           OK
THE MODEL PUT OUT WARNINGS:
FieldPlanting: adjusting area per plant from 1600.000000 to 1752.600000      1x
threshingrootDiameter                                                   294226x
Simulation took (hours:minutes:seconds): 0:18:18
```

Looks like the models run successfully, and neither of these warnings indicate any error -- the fieldPlanting module correctly recalculates area per plant from the planting scheme, and `threshingrootDiameter` comes from the root length thresholding function that reports the root length in each core for specified diameter classed and is issued each time the threshold function is called -- looks to me like a forgotten debug output.

Opened boths `roots040.00.vtp` in Paraview, inspected root architecture visually. Root angles look good to me; sent screenshots to Jimmy for approval.

Also in Paraview, colored roots by their length within any core (Python calculator with expression `rootSegmentLengthInCore_L1_tot + rootSegmentLengthInCore_L2_tot + ... + rootSegmentLengthInCore_L6_tot`), verified that cores show up at the expected locations. Cores 1,2,3,6 are all where expected; cores 4 & 5 (the midrow locations) have no roots in them for this simulation of either maize or bean (verified by checking `coringData_L*_tot.tab`) and therefore aren't visible by coloring roots.

For bean, this isn't surprising -- the midrow cores are taken at z=38.1, but there are no roots from any plant between z=27 and z=44.8. In maize, however, it appears both cores *would have* encountered roots if they went deeper than 60 cm. We can confirm this in Paraview:

* Select roots, Filters > Clip
* Clip type: Cylinder, center at 11, 0, 38.1 (=location 5), radius 1.11
* Select this clip, Filters > Clip
* Clip type: Plane, origin at 11, -60, 38.1, normal 0, 1, 0
* Select this clip, Filters > Python Calculator
* expression = `sum(rootSegmentLength)`
* invert plane clip to toggle between length above and below 60 cm

For maize L5, I get 139.9 cm of root below 60 cm and 0 cm (as expected) above it. For L4 (0, 0, 38.1), 0 cm above 60 and 5.6 below... *EDIT* the virtual cores in OSR are 2.22 cm *radius*, not diameter, so the virtual cores would have encountered more roots than this clipping does. But I'd already closed Paraview by the time I noticed, and the basic point still stands.

Now checking tabled output. To save space, code only shown here for maize; bean is identical except reading from "beantest" and using `bn` instead of `mz` in variable names throughout.

```{r}
library(tidyverse)
mz = read.table(
	"maizetest/tabled_output.tab",
	header = TRUE,
	stringsAsFactors = FALSE,
	strip.white = TRUE)
```

Total root length in each core over time, split by diameter:

```{r}
(mz
	%>% filter(path == "//fieldCoring")
	%>% separate(name, c("loc", "diam"), "_")
	%>% ggplot(aes(time, value, color = loc))
		+ geom_line()
		+ facet_wrap(~diam))
```
Maize cores ranked from high to low root length at day 40: L6, L1, L2, L3, L4 & L5 both zero. Between-core differences are generally consistent across diameter classes, and length in most cores rises fairly monotonically with time. Exception: L3 plateaus between day 20 and 30 while other cores keep rising. Nearly all roots are in 0.2-0.5 mm size class, very few roots > 0.5 mm or < 0.2 mm in any core.

Bean core length ranks in pairs: L2 & L6 highest length, L1 & L3 about 2/3 as much, L4 & L5 both zero. All cores with any roots show roughly the same monotopic trajectory. Higher fraction of roots < 0.2 mm in bean (10-25% of total) than maize, still very few > 0.5 mm.

Depth indices (D50, D80, D90, D95):

```{r}
(mz
	%>% filter(grepl("D\\d", name))
	%>% ggplot(aes(time, value, color=path))
	+ geom_line()
	+ facet_wrap(~name, scales="free_y"))
```

 Turns out each plant reports these separately! plant 1's D50 is consistently a cm or so shallower than other 3, but no difference between plants in other indices. All indices show same trajectory: linearly deepening except for a brief shallowing digression around days 5-10 as the first nodal roots emerge.

Edit: DO NOT TRUST these indices yet. See 2019-04-01 below.

root depth distribution
```
(mz
	%>% filter(grepl("RootLengthProfile", name))
	%>% extract(name, "depth", "_(\\d\\d)[-+]", convert = TRUE)
	%>% ggplot(aes(depth, value))
		+ geom_point()
		+ geom_smooth()
		+ coord_flip()
		+ scale_x_reverse()
		+ facet_wrap(~time))

mz_cores <- (
	tibble(
		file = list.files(
		path = "maizetest/",
		pattern = "coringData*",
		full.names = TRUE))
	%>% mutate(
		data = map(
			file,
			read.table,
			header = TRUE,
			stringsAsFactors = FALSE,
			strip.white = TRUE))
	%>% extract(file, c("location", "diam"), "coringData_L(\\d)_(.*).tab")
	%>% unnest(data)
	%>% rename(time = time.d., coreDepth = depth)
	%>% gather(
		key = "depth",
		value = "value",
		starts_with("X", ignore.case = FALSE))
	%>% extract(
		depth,
		c("depth_top", "depth_bot"),
		"X(\\d+)\\.(\\d+)",
		convert = TRUE))
```

Plotting root length in cores against whole-plot root length. To put these on the *approximate* same scale as each other, I'll multiply each core by the ratio of core area to whole-plot area: `(114.3+38.1)*(11.5+34.5) / (2.22^2*pi)` = 452.78. If this comparison showed all cores plotting near the whole-plot line (it won't!), we could think of this as support for the (unreasonable) hypothesis that root length is evenly distributed throughout the plot.

```
(mz
	%>% filter(grepl("RootLengthProfile", name), time %% 5 == 0)
	%>% extract(name, "depth", "_(\\d\\d)[-+]", convert = TRUE)
	%>% ggplot(aes(depth, value))
		+ geom_point()
		+ geom_smooth()
		+ coord_flip()
		+ scale_x_reverse()
		+ facet_wrap(~time)
		+ geom_smooth(
			data = filter(mz_cores, diam=="tot", time %% 5 == 0),
			aes(depth_top, value*452.78, color = location)))
```


## 2019-03-21

Response from Jimmy on updated root architecture: Maize looks good, and bean looks like the *OSR output* he's seen before, but it still doesn't look like a *Carioca root system* to him, because the roots turn downward too fast and too uniformly. Jimmy says when he takes cores midrow between Carioca plants 40 DAP, he typically sees competition between roots from opposite rows in the top 20 cm --> he thinks we're much more likely to get realistic results if I can tweak gravitropism such that the midrow soil contains at least *some* roots less than 20 cm deep, and allow more randomness in the growth pattern.

Adjusted bean parameters by widening the range of allowable random values for root classes where `gravitropism.v2` is a SimulaStochastic (primary root, all 4 basal whorls, hypoctyl borne) until I saw roots in the top 20 cm of the midrow region. Did not yet change gravitropism of laterals, which is currently SimulaConstant 0 in all classes.

Note that maximum gravitropism values for all the axial roots are now greater than zero, i.e. the net gravitropic effect in a given timestep is allowed to be an upward rather than downward force. However `cannotgrowup` is true for all these classes, so the *overall* root growth direction will never go above horizontal, but gravitropism is allowed to more-then-overcome the downward effect of other tropisms. Biologically this isn't the most sensible (we might prefer to think of gravitropism as constant and the fluctutations as coming from other effects), but:

* The model just adds up all the tropism offsets, so it doesn't care which term the randomness came from.
* The current implementation of gravitropism is far from mechanistic, so this doesn't affect the interpretability of the parameter values (we're not even sure whether they have an identifiable unit) or how we tune them (they're purely empirical either way).
* And this way I only had to tweak one parameter.

While I was editing, I also removed all `gravitropism` parameters from the bean file. These were being ignored anyway -- all root classes already have a `gravitropism.v2` sibling, which OSR uses preferentially.


## 2019-03-25

Jimmy says updated bean architecture looks much better and we can proceed with it. Next steps: Send current outputs to Jagdeep for analysis development, set up for replicated runs on cluster.


## 2019-04-01

**DO NOT TRUST** the D95 (or D90, D50, etc) values from current test simulations. In exploratory work Last week I found two bugs in OpenSimRoot's D95 calculation:

* The apparent difference in D50 between plant 1 and others that I remarked on above is not real: Each timestep, the first invocation of OSR's D95 function finds fewer root nodes than any subsequent calls, and the calls are made in alphabetical order by name (So `D50` of plant `36H56_1_1` comes before e.g. `D95` of plant `36H56_2_1`). It appears to be possible to work around this by adding an extra D95 call with a name like "000000_D95_DUMMY" that we ignore during analysis, but I'm investigating more elegant fixes.

* When simulating multiple plants as we are here, the D95 calculation is run separately for each plant by adding up roots from deep to shallow until it has seen `(1-threshold) * rootLength`, but it does it by incorrectly looping over the roots from *all* plants, so this means all D95 calculations from this simulation will be much too deep -- they're really the depth below which root length from all four plants is equal to five percent of this plant's total root length. This affects all caclulations, no matter what threshold they use or what order they're called in. I'm working on a fix for this in OSR and will make a note here when it's done.


## 2019-04-17

### D95 fixed for multiple plants
The D95 calculation bug for multiple plants is fixed in OpenSimRoot source code -- compile it from branch `D95-fixes`.

## D95 first-invocation workaround
First-invocation bug is [reported](https://gitlab.com/rootmodels/OpenSimRoot/issues/33) but not yet fixed. The underlying issue seems to be that D95 takes its numerator from a loop over the existing root nodes (which does not include growthpoints) and its denominator from the whole-plant root length (`rootLongitudinalGrowth` if present or `rootLength` if not, both of which include length accumulated "inside the growthpoint" and not yet assigned to a node). When there is a large amount of root length in growthpoints, this discrepancy adds up.

The reason this specifically affects the first D95 call each timepoint is that the D95 calculation loop gets the length of each segment using calls to `rootSegmentLength->get()`, which trigger updating routines on the roots they touch, which convert length in growthpoints to length in new root nodes, which the next D95 call will then find. So *if* the root hasn't been updated at the time D95 visits it, one D95 triggers creation of new root nodes that the next D95 will see. But beware that D95 only loops over as many root segments (in order from deepest to shallowest) as it takes to accumulate (100-threshold)% of the total plant root length, so this still doesn't guarantee complete updating.

Not only is the bug not yet fixed, it's now more of a problem with the multiple-plant bug fixed: The model now *fails* when calculating D50 very early in the simulation, because the sum of the nodes it finds is less than 50% of the length it's expecting. For bean it's sufficient to add a dummy D95 call named in a way that it comes alphabetically before the D50 call, but for maize the crash persists unless I also turn off `rootLongitudinalGrowth` (which is tracked cumulatively for each root based on growth rates) so that D95 instead takes its denominator from `rootLength` (which sums lengths over every root segment every time it is called, therefore guaranteeing (I *think*) that all nodes get updated).

==> As a temporary workaround, made the following changes in both maize and bean:

* Added a duplicate D95 call, with name `00_initial_D95` so that it comes alphabetically first, meaning the depth indiced calculated at each timestep are now `00_initial_D95`, `D50`, `D80`, `D90`, `D95`.
* Commented out `//plantTemplate/rootLongitudinalGrowth`.

### Silencing table extrapolation warnings
In updating to the `D95-fixes` branch, I also pull in a number of other recent changes to the OpenSimRoot code.
One annoying side effect: new warnings about `extrapolation at end of table` from the `rootSegmentLengthInCore` tables. As far as I can tell this warning appears *any* time a SimulaTable is extended by more than a tiny timestep at once, and the "extrapolation" here is really just writing of accurately calculated values. Silenced the warning by changing the initial value of each empty table from "0 0" to "0 0 1000 0" and confirmed that output remains identical.

## 2019-04-18

### Reduce noise in tabled output

Accidentally committed this to bean file yesterday (in same commit as D95 workaround), decided to keep it and add to maize file too: The tabled output contains a *ton* of repeated lines for "output" variables that are really constant *inputs* to the coring and root profile modules. We really don't need to see that `radius` = 2.2225 each of 41 days * 6 locations * 4 size classes = 984 times, especially since it's available in the `CoringOutput*.tab` files as well! To fix this, added the variables I want ignored to `//simulationControls/outputParameters/table/skipTheseVariables`. Was "primaryRoot, hypocotyl", now "primaryRoot, hypocotyl, threshold, y1, y2, coringDepth, radius, verticalSpacing, maxThresh, minThresh".

### Extend root profile all the way down

Added more layers to `//soil/rootLengthProfile*`, to report 10-cm layers all the way from 0 to 150 instead of lumping together all roots below 90 cm.

### Add randomly-placed cores

The field experiment for this project was designed with a systematic sampling approach, but as long as I'm running the simulations I'd also like to compare the results from a larger number of *randomly* placed cores. Added a new `//randomCoring` section to the input file, immediately after the existing `//fieldCoring`, containing 15 cores that are, like the fieldCoring cores, 2.2225 cm in diameter and reported in 10-cm segments from 0 to 60 cm depth. These report total root length only -- did not include any reporting of separate diameter classes.

I want each core's location within the plot to be a random draw from a uniform distribution that covers the whole plot area, which doing this with OSR turns out to be harder than I expected: `<simulaStochastic type="coordinate">` draws its values for all 3 axes from the same distribution with the same limits, so in a rectangular plot I would either need to limit sampling to a square subplot or expand it to a square *larger* than the plot and accept zeroes from the cores that fall outside the plot.

Spent some time investigating ways to construct a `SimulaConstant<Coordinate>` from separate inputs, so that I could write something like...

```
<SimulaConstant name="center" type="coordinate">
	<SimulaStochastic name="x" distribution="uniform" minimum="-5" maximum="15" />
	<SimulaConstant name="y">0</SimulaConstant>
	<SimulaStochastic name="z" distribution="uniform" minimum="-38" maximum="114" />
</SimulaConstant>
```

...but eventually decided to solve the problem outside OSR. Set all the `randomCoring/*/center` elements to `-100 0 -100` in the XML files, then wrote a small R script `randomize_cores.R` that takes the XML file as input, replaces the values of all the `//randomCoring/*/center` elements with new uniform draws from the X and Y dimensions, and writes the modified file back to disk with a name the user specifies. Each run of the model will now go something like `./randomize_cores.R bean.xml bean_r.xml && OpenSimRoot bean_r.xml`.

### Set up batch runs

To run a big pile of these simulations, I'll want to submit them to the PSU cluster and to set random seeds for reproducibility. Expanded `randomize_cores.R` to become a complete run setup script, renamed to `set_up_runs.R`. It now takes three arguments (name of the input file to use, name of the output directory to put the results in, and the number of replicates to create) and when run it generates subdirectories for each model, writes a randomNumberGeneratorSeed to each input file along with the randomized core locations, and creates a `run.sh` that should work to run all of the generated models on the open queue of the PSU ACI cluster with a single submission[1].

[1] well, a single submission unless there are more than 100 replicates, in which case the open queue won't allow starting them all at once, but the same run script should work if submitted in several stages, e.g. 150 jobs with `qsub -t 1-99 run.sh`, wait for these to finish, then `qsub -t 100-150 run.sh`.

### No VTP output

For the large number of runs I want to use for evaluating random core placement, VTP output will be too large even at 20-day interval. Turned off in these files, can turn it back on for future local runs as needed.

### Let's run some models!

Local test run of maize.xml takes 11:37 and uses 3.7 GB memory. Bean takes 6:44 and uses 1.4 GB. Edited PBS parameters in run.sh to request 1 processor, 30 minutes walltime, and 6 GB memory per job.

Set up 500 replicates each of bean and maize:

```
./set_up_runs.R bean.xml bean_20190418 500
./set_up_runs.R maize.xml maize_20190418 500
tar czvf Burridge_coring_20190418_inputs.tgz bean_20190418 maize_20190418
scp Burridge_coring_20190418_inputs.tgz ckb23@datamgr.aci.ics.psu.edu:work/Burridge_coring_20190418_inputs.tgz
```

Logged in to ACI-i, compiled the patched version of OpenSimRoot, set up working directory:

```
cd ~/OpenSimRoot/OpenSimRoot/StaticBuild
module load git
git checkout -b D95-fixes infotroph/D95-fixes
module load gcc/7.3.1
make -j12 all
mv OpenSimRoot ~/bin/OSR_D95fix_af8addf_20190418
module purge
mkdir ~/work/Burridge_coring_20190418
cd ~/work/Burridge_coring_20190418
cp ~/bin/OSR_D95fix_af8addf_20190418 .
tar xvzf ../Burridge_coring_20190418_inputs.tgz
```


Logged in to ACI-b, ran models!

```
cd work/Burridge_coring_20190418/bean_20190418
qsub -t 1-100 run.sh # 18:31 local time, complete 18:41
qsub -t 101-200 run.sh # 18:55, complete 19:08
qsub -t 201-300 run.sh # 19:16, complete 19:26
qsub -t 301-400 run.sh # 19:54, complete 20:02
qsub -t 401-500 run.sh # 20:49, complete 20:59
cd ../maize_20190418/
qsub -t 1-100 run.sh # 21:10, complete 21:25.
# ^ maize_23 doesn't appear in qsub log at start, but does at completion and output is in the correct folder.
qsub -t 101-200 run.sh # 21:34
# ...submitted remaining maize batches from my phone, did not track start/end times
```


## 2019-05-07

Spent some time last week working toward adding variant root architectures, but didn't record my progress until now.
The basic idea is to generate maize and bean models that have a range of root system shapes; initially we want a "shallow", "intermediate", and "deep" phenotype of each, but may later want to expand this to a continuous gradient.

The tentative plan:

* Alter maize root architecture by varying the branching angle of nodal and brace roots, probably by taking our current maize parameters as intermediate and adding 30 degrees to each whorl for the deep phenotype and subtracting 30 degrees for the shallow phenotype.
* Bean basal roots already emerge at 90 degrees (-horizontal), so there's not much room to make them shallower by varying branching angle. Instead, adjust gravitropism -- weakening (= making maximum and minimum both less negative) by 0.01 for the shallow and strengthening (=more negative) by 0.03 for deep look right to me; waiting for feedback from Jimmy while I write this.
* Rather than keep track of multiple input files for each crop, alter phenotypes on the fly in `set_up_runs.R`.

Altering phenotypes on the fly requires a new input to specify the parameters that change between phenotypes: `parameter_changes.csv` contains one line for each parameter that needs to be updated to create each phenotype, and has the following columns:

* `source_file` gives the name of the XML file to use as the baseline configuration. For this experiment, this is either "maize.xml" or "bean.xml".
* `output_name` gives a name for the resulting model parameterization / genotype, e.g. "maize_deep".
* `path` gives the slash-separated XML path to the parameter to be changed, e.g. "nodalroots4/branchingAngle". If the path exists in multiple places in the file, they will all be updated to the same value, so if you only intend to replace one then be sure to specify enough of the path to identify it uniquely.
* `attribute` gives the XML attribute to be updated, or "text" to update the contents inside the tag. E.g. to change `<SimulaBase name="x" unit="cm">5</SimulaBase` into `<SimulaBase name="x" unit="mm">6</SimulaBase>`, the CSV should contains two lines with the same `path` and `output_name`: One with `attribute` set to "unit" and `value` set to "mm", the other with `attribute` set to "text" and `value` set to "6".
`value` gives the text to use for the updated parameter or attribute.

Any other columns in the file are ignored, which is useful to allow tracking other parameter details in the same file, e.g. a `citation` column tracking the source for each parameter value. Additionally, all text after a hash mark ("#") on any line is skipped; use this to leave comments or to temporarily disable certain lines.

When `set_up_runs.R` is run, the rows matching each unique combination of `source_file` and `output_name` is processed in one pass by updating all the parameters given (while keeping the values from `source_file` for all parameters *not* given), then repeatedly altering random variables (coring locations, RNG seed) to generate the number of replicates requested in the script arguments. Note that not every genotype needs to change the same parameters; it would be fine for `parameter_changes.csv` to contain one line for genotype `bean_mostly_stock` and then a hundred lines for `bean_very_changed`.


## 2019-05-08

### Avoid array jobs

While asking ACI helpdesk staff about what I thought was an unrelated question, learned there's an ongoing intermittent issue in the closed-source interface between the cluster's scheduler (Torque) and resource manager (Moab), such that array jobs (i.e. anything that passes a `-t` argument to `qsub`) don't always get passed around properly. More details are in CKB's email )(search for "ICS i-ASK" and "ticket 39561") but in short ACI staff currently recommend against using array jobs at all. They suggest instead writing one bash script that loops over the list of models and submits each one as its own separate `qsub` job.

I decided to take this as an excuse, and overhauled `set_up_runs.R` to generate a launcher script which monitors queue length and periodically submits more jobs to refill the queue without exceeding the user quota of 100 jobs waiting+running at any given time. Each job submits the same run script (`run_model.sh`), with the input file passed as an environment variable named `$file`. Example: `qsub -v file=bean.xml run_model.sh`. I'd prefer to just pass the file as a positional argument (i.e. I wish I could write `qsub run_model.sh bean.xml`), but the ACI version of `qsub` doesn't support passing command-line arguments to scripts.

This change was a hassle, but should be a step up from the old system (he writes, having not yet test-run it!) because I can start batches of >100 models with a single command and leave them to run unattended. As long as the whole batch doesn't take more than 48 hours (the longest allowable wall time and hence the longest one invocation of the launcher script can go), anyway.

### Variant architectures: just vary angle

After discussions with Jimmy on how exactly to parameterize the deep-medium-shallow architecture variants, we eventually settled on only changing the branching angle of main root axes -- it's simple to set, simple to understand, and has a major effect on the visual shape of the root system. For bean, we'll take the current standard architecture as "shallow" and create the deeper phenotypes by adding 20 degrees ("intermediate") of 40 degrees ("deep") to the branching angle of each basal and hypocotyl-borne root whorl. For maize, we'll take the current standard architecture as "intermediate" and either add ("deep") or subtract ("shallow")
30 degrees from each nodal or brace root.

During discussions, we also considered altering bean gravitropism, but decided that would be more complicated to explain while likely producing phenotypes that behaved substantially the same, with respect to how they look in cores, as those produced by altering branching angle. I'm leaving the gravity changes commented out in `parameter_changes.csv` for easy re-enabling if we decide to revisit this again later.

### Generate variant-architecture inputs

Let's see if this works! I'll start with 100 replicates of each model.

```
./set_up_runs.R parameter_changes.csv Burridge_coring_20190508 100
tar czf Burridge_coring_20190508_inputs.tgz Burridge_coring_20190508
scp Burridge_coring_20190508_inputs.tgz datamgr.aci.ics.psu.edu:work
```

On cluster:

```
cd work/
tar xf Burridge_coring_20190508_inputs.tgz
cd Burridge_coring_20190508
qsub submit.sh
```

Script launches `bean_1`, then exits with no error message rather than continue launching other jobs. The bug: `((var++))` has unintuitive return semantics that interact strangely with `set -e`, such that when `$i` equals 0 on the first model `((i++))` returns 1 and therefore stops the script. Changed it to `i=$(( i + 1))`, reran the run setup steps above from `./set_up_runs.sh ...` through `qsub submit.sh`.

599/600 models ran to completion, but `maize_deep_100` exceeded the 30-minute walltime limit and was killed by the scheduler just *barely* (it was writing the first few lines of the day 40 output when killed!) before completing. 36 other maize models take >20 minutes, but finish in time.

Reran the one failed model with a manually increased walltime limit: `qsub -l walltime=01:00:00 -N maize_deep_100 -v file=maize_deep_100/maize_deep_100.xml run_model.sh`. Also updated `run_model.sh` to reserve 1 hour for future runs.

Packed up all results (`tar czf Burridge_coring_20190508_outputs.tgz Burridge_coring_20190508`), copied back to my machine (`scp datamgr.aci.ics.psu.edu:work/Burridge_coring_20190508_outputs.tgz .`) for analysis and archiving on Box.


## 2019-05-09

Updating my exploratory plotting of the fixed and random core locations to include variation in root angle. This means `plot_random_cores.R` will no longer work as written on the 2019-04-18 output; to remake plots from that run, grab the old version from Git history: `git show d528a86b:plot_random_cores.R > plot_random_cores_20190418.R`.


## 2019-08-01

Added more detail to exploratory plotting; mostly looking at calculating error terms for the across-layers differences between fixed cores and whole-plot RLD.  `plot_random_cores` isn't a very descriptive name anymore; renamed to `exploratory_plots_compare_positions.R`.


## 2020-01-27 through 2020-02-04, in short bursts

Implementing a bootstrap analysis to answer the question "how reliably can we detect differences in rooting depth at each coring location?"

The basic idea: repeatedly sample n simulations from each parameterization, calculate the apparent D95 from each core, then within each coring location perform a one-way anova on D95 ~ genotype and record whether we would have rejected the null. If the simulated difference in D95 is big enogh to be of practical interest, then the smallest n which reliably rejects the null is the number of replicates from that coring location needed to detect a difference of that size in the field.


### Implementation notes

* Broke out the data-reading steps from `exploratory_plots_compare_positions.R` into reusable functions, moved them to `data_readers.R`

* Using my own D95() function, *not* soilCoreTools::DX(). Mine follows Jouke's advice to integrate before interpolating (or, in English: To fit a curve through the depth profile of cumulative root length, rather than fitting to the density of individual layers and then summing the estimates). This seems to work better for cases where the observed root length isn't monotonically decreasing with depth. An example of where `soilCoreTools::DX` fails and mine seems to behave better: With root lengths of 94 + 2 + 0 + 4 + 1 in layers of any arbitrary thickness, we can note that we pass 95% of cumulative observed length somewhere in the second layer. But DX puts it in the third layer:

	```
	soilCoreTools::DX(lengths=c(94, 2, 0, 4, 1), depths=c(0, 10, 20, 30, 40))
	# [1] 29.54774
	# rearranging root length below D95 not only affects estimate,
	# but moving roots deeper makes estimate get shallower!
	soilCoreTools::DX(lengths=c(94, 2, 0, 1, 4), depths=c(0, 10, 20, 30, 40))
	# [1] 28.74372
	# Integrating before interpolating, as my and Jouke's implentations do,
	# means predicted D95 stays in same layer as observed
	# (after accounting for top-of-layer vs bottom-of-layer depth specification)
	D95(rootlen=c(94, 2, 0, 1, 4), depth=c(10, 20, 30, 40, 50))
	# [1] 19.8
	# rearranging roots below D95 doesn't affect estimate
	D95(rootlen=c(94, 2, 0, 4, 1), depth=c(10, 20, 30, 40, 50))
	# [1] 19.8
	```

* On inspecting the raw core data (plotted in `raw_core_metrics.png`), it's evident that many locations tend to have estimated D95 very close to 60 cm, i.e. the bottom of the core. Since the plants root much deeper than the cores (whole-plot D95s range from near 65 for bean shallow to 100-110 for maize shallow -- yes, maize shallow has a deeper D95 than maize deep!), this isn't a surprise, but this pushes all the data up against a ceiling and therefore could affect the analysis by reducing differences between genotypes. To examine this in more detail I'll perform the resampling in parallel on all the depth metrics recorded for whole plots: D50, D80, D90, D95. Whole-plot D50 is always less than 35 cm in all genotypes, so at least that one should have enough variation to give useful information.

* When a core contains no roots at all, my D95 function returns 0... which I guess isn't *wrong* ("by the time you pass the soil surface you've already seen more than 95% of your zero roots"), but it's not useful for this analysis and skews the depth comparisons. In our 100 simulations only three crop x genotype x location combinations contain any zero-root cores, and none of these groups are more than 8% zeroes (==> at least 92 simulated cores left to work with), so I'll drop cores with no roots from the dataset before resampling.

* I'll compute the rate of H0 rejection for sample sizes of 2, 3, 5, 8, 12, 15, or 20 cores.

* In addition to the six fixed coring locations, I'll also resample from the d95 of the whole simulated plot and from one of the 15 randomly-positioned virtual cores. Since all 15 random cores are drawn from the same distribution and independently placed in each OSR run, sampling from any one per simulation is equivalent to sampling from any other of them.

* To do the resampling, I'll use the functiom `mc_cv` from the `rsample` package, because it's easy (for me!) to use and it works nicely in a tidy-data framework. `mc_cv` works on  the concept of "splits": You provide a dataset and ask for a number of resamples, and each returned resample consists of an "analysis set" (made of rows drawn from the data according to the procedure and sample size you asked for) and an "assesment set" (made of the rest of the data; for our purposes today we'll ignore this). I haven't looked up the exact details of how this is implemented, but importantly rsample does not duplicate the entire dataset in memory every time (otherwise we'd be storing 32000 copied datasets!). The basic usage pattern is to compute a set of splits, then fit your model to the analysis split of each sample:
	```
	fit_my_model <- function(rsplit, ...){ tidy(some_model_fn(..., data = analysis(rsplit)))}
	mv_cv(data, ...) %>% mutate(model = map(splits, fit_my_model, ...))
	```

* NOTE: Cores will be sampled without replacement within each split, so e.g. the value from OSR replicate #59 will never be selected twice in a given resampling. When I first chose to use `mc_cv` I incorrectly believed it was sampling *with* replacement, have now realized my error, but don't think it makes enough difference to rewrite the sampling functions. If we decide we *do* want to sample with replacement, probably the easiest route is to generate splits using `rsample::bootstraps` (which samples with replacement but always produces an analysis set the same size as the original data) and then subset each split to the desired size in the model-fitting function using e.g. `dplyr::sample_n`.

* Since each model fit wants to be comparing D95 (or D50, etc) between root angles inside a given crop/location/sample size, I'll resample independently from each one:
	- Start with a set sample size, e.g. n=2
	- Break the data up into chunks by crop and location, but with each chunk containing all root angles
	- Pass each chunk of (3 angles * 100 simulations) = 300 values (modulo any missing zero-root cores) to a separate `mc_cv` call, which will resample 2000 times.
	- Each resampling selects `n` rows from the passed chunk, stratified by root angle so that we get equal numbers of samples from each genotype.
	- fit `aov(D[x] ~ root_angle)` to all four depth metrics, using the analysis set from this resampled split as data.
	- Record the p-values of these models and go on to the next chunk.
	- This gives (2000 resamples * 2 crops * 8 locations) = 32000 rows per sample size, each containing 4 p-values.
	- Repeat this whole procedure separately for each sample size, for a total of 224000 virtual coring experiments.


### null rejection rate

On my laptop, the resampling and model-fitting takes around 45 minutes. It would probably run faster if I defined strata on the full interaction of crop/location/angle and resampled from all at once, but then I'd need to account for these groups inside my model-fitting function.

The primary output from the resampling: fraction of the 2000 model fits at each sample size/location that reject H0 and conclude there are differences in D[x] between the genotypes. Full data saved in `null_rejection_rate.csv`, and much more digestably plotted as `null_rejection_rate.png`. Notable takeaways:

* D50 rejects the null much more often than others in nearly every condition, with the strange exception of L2 bean where the order is inverted. No coring location reaches 100% null rejection in any metric
* If you're excavating Whole-plot monoliths of bean, 2 samples are enough to always reject the null. With cores, you need many more -- with 5 or fewer samples, no coring location has more than about 70% rejection, and that only with D50. Even with 20 samples, only D50 saturated to 100%... with the strange exception of L2 maize
* L2 is *weird* compared to the other locations: In bean D95 has the highest rejection (of only 20%  at n=20) and D50 the lowest (single-digits at every sample size). In maize, all four metrics plot basically on top of each other, saturating from 25% at n=2 to effectively 100% by n=15.
* Maize genotypes really do not differ much, especially in D80. Even whole-plot samples aren't guaranteed to reject the null when n=2, and d80 doesn't reach 100% until n=12.
* In bean, a random sampling strategy is basically as effective as any of the fixed locations. L4 and L5 increase their rejection rate slightly faster, but at high sample sizes they're nearly equivalent.
* In maize, random sampling is less efficient than any of the fixed locations -- For any metric, the rejection rate it gets at n=20 (20-30% depending on metric) is equalled or surpassed n=8 in any of the fixed locations.

### resampled group means and differences

I briefly explored using `TukeyHSD` to record estimated pairwise differences between root angles for each model fit, but decided instead to move these to a separate analysis: I reran the same sampling scheme as above, swapping out the model-fitting function to compute the mean D[x] of each genotype and the difference between these means. If the interval containing 95% of these differences doesn't include zero, we can conclude that we're reliably detecting a difference between those genotypes under that condition of crop/location/sample size.

Since this calculation can support it, I added another sample size of n=1, thus bringing us up to a total of (224000+32000) * (4 metrics) = 1,024,000 estimates, each containing 3 group means and 3 differences of means! The resampling takes a few minutes longer than before (closer to 50 than to 45), but once again is fast enough that I don't feel like it needs further streamlining.

The resulting dataset now has at least five dimensions (crop, genotype, location, sample size, depth metric) plus s further choice between looking group means or the differences between them. This is a challenge to visualize. For the moment I've broken it into four figures:

* `d50_estimates.png` and `d95_estimates.png` show only one metric per figure, with facets for location and crop, genotypes as factors on one axis, and sample size as overlapping violins of different colors. In the end I only plotted five sample sizes: one is the raw data without resampling (n=1 looked very similar to this, as expected), then n=(2, 5, 12, 20). The trend of reducing uncertaintly is clear from these and it should be easy to infer the other sample sizes by interpolating between them. I did not save any plots from D80 or D90, though they'd be easy to make if we need them.

* `d50_diff_ests.png` and `d95_diff_ests.png` also show only one metric per figure, with facets for sample size and location, crops as factors on the x axis, and violins of different colors for each comparison between genotypes. The y-axis is difference in estimated genotype mean, and you can do a pretty good visual check for "reliably detecting a difference" by looking for violins that don't cross the zero line. This is overconservative (the violin tails can go past the 2.5/97.5 quantiles), but a good start.

* Additionally, I computed the 95% intervals for every comparison, highlighted those that don't include zero with an asterisk in a corresponding "nonzero" column, and saved the whole mess as `resampled_genotype_diff_intervals.csv`. Might eventually be nice to revisit plotting it, but meantime it's available for anyone who wants to read it.


## 2020-02-05

Realized I've made the resampling much more awkward than it needs to be, by comparing Dx from all depths of whole plots against Dx from only the 0-60 layers of cores. OSR-computer Dx is only available for the full simulation depth, but I can compute a whole-plot 0-60 cm Dx by getting whole-plot values by layer from the tabled output RootLengthProfile entries, then filtering out all layers > 60 cm and computing D95 myself. Then all the resampling would happen on comparable scales, the figures showing estimated group means would allow direct comparisons of questions like "does this location match the whole plot", the "maize shallow actually has a deeper D95" confusion goes away, and the OSR-calculated full-depth Dxs would become a passing sentence in the results or discussion: "note that a 60 cm depth only capures about 75% of the total simulated maize root length and 80-90% of total simulated bean root length, but to match the depth limitations common in field sampling we consider here only the depth metrics calculated from the 0-60 cm layers of the simulation"

...Yes, that sounds great, but I've already taken way longer to finish this analysis than I told Jimmy it would be, so putting this on the list for future refinements and sending it around to colleagues in its current form, with whole-plot metrics extending to 150 cm.

Evening of 2020-02-05: Went ahead and updated code to compute whole-plot Dx from top 60 cm only, adjusted plots to remove 60-cm indicator line that will now be at the bottom margin, left to run overnight


# 2020-02-06

Script ran in 1h 31m  with no errors. Looking at updated figures:

* Yep, figures are much easier to interpret now.
* Raw core metrics now consistently show the expected deep-medium-shallow trend at every location, no matter which Dx we look at.
* Range of raw whole-plot Dxs is smaller now, as expected, e.g. D95 violin for maize shallow used to span about 100-110 cm, now 52-54.
* Variance of whole-plot D95 used to be higher than variance of whole-plot D50, now more or less reversed. This is consistent with the "pushed against the ceiling" hypothesis.
* Null rejection rate for whole maize plots is now 1 for all n>=3, and for 2=n the lowest is 0.98 for D95.
* random coring now comes close to matching whole-plot D50 -- the whole plots are always within the tails of the n=20 random violins. For D95 random coring ests are about 10 cm too shallow.
* Notice that even with n=2 the vairance in whole-plot Dx estimates is now TINY. To some extent this isn't a surprise, but I also wonder whether this means the interesting differences in depth are driven by the deepest roots, or whether it's a more mechanical issue that a D95 calculated from 10-cm layers is inherently less sensitive than one calculated from individual roots as OSR does.

Want to think more about this, but meanwhile sent the updated plots to Jimmy for inclusion in today's draft of the manuscript.
